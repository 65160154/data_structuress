public class Graph {
    private final int MAX_VERTS = 20;
    private Vertex vertexList[];
    private int adjMat[][];
    private int nVerts;
    private Queue theQueue;

    public Graph() {
        vertexList = new Vertex[MAX_VERTS];
        adjMat = new int[MAX_VERTS][MAX_VERTS];
        nVerts = 0;

        for (int j = 0; j < MAX_VERTS; j++) {
            for (int k = 0; k < MAX_VERTS; k++) {
                adjMat[j][k] = 0;
            }
        }

        theQueue = new Queue();
    }

    public void addVertex(char lab) {
        vertexList[nVerts++] = new Vertex(lab);
    }

    public void addEdge(int start, int end) {
        adjMat[start][end] = 1;
        adjMat[end][start] = 1;
    }

    public void displayVertex(int v) {
        System.out.print(vertexList[v].label);
    }

    public int getAdjUnvisitedVertex(int v) {
        for (int j = 0; j < nVerts; j++) {
            if (adjMat[v][j] == 1 && !vertexList[j].wasVisited) {
                return j; // Return the first unvisited adjacent vertex
            }
        }
        return -1; // No such unvisited vertices
    }

    public void bfs() {

        vertexList[0].wasVisited = true; // Mark the starting vertex as visited
        displayVertex(0); // Display the starting vertex
        theQueue.insert(0); // Insert the starting vertex at the tail of the queue

        int v2;

        while (!theQueue.isEmpty()) {
            int v1 = theQueue.remove(); // Remove the vertex at the head of the queue

            // Process all unvisited neighbors of v1
            while ((v2 = getAdjUnvisitedVertex(v1)) != -1) {
                vertexList[v2].wasVisited = true; // Mark the neighbor as visited
                displayVertex(v2); // Display the neighbor
                theQueue.insert(v2); // Insert the neighbor at the tail of the queue
            }
        }

        // Queue is empty, so we're done. Reset all visited flags.
        for (int j = 0; j < nVerts; j++) {
            vertexList[j].wasVisited = false;
        }
    }

}
